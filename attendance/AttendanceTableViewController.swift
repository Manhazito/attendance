//
//  AttendanceTableViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 09/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class AttendanceTableViewController: UITableViewController {
    
    var athletes = [Athlete]()
    var session: TrainingSession?
    var dojo: Dojo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let session = session else {
            fatalError("No training session loaded!")
        }
        
        var allAthletes = session.missedAthletes.union(session.partialAthletes).union(session.presentAthletes).union(session.watchedAthletes)

        if let dojo = dojo, allAthletes.isEmpty {
            allAthletes = dojo.athletes
        }
        
        athletes = Array(allAthletes)

        tableView.tableFooterView = UIView(frame: .zero) // Hide empty cell lines
    }
    
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? 1 : athletes.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "athleteTableHeaderCell", for: indexPath)
            
            guard let headerCell = cell as? AttendanceTableViewHeaderCell else {
                fatalError("The dequeued cell is not an instance of AthletesTableViewHeaderCell.")
            }

            let dateFormatter = DateFormatter()
            let timeFormatter = DateFormatter()
            dateFormatter.dateStyle = .long
            dateFormatter.timeStyle = .short
            timeFormatter.dateFormat = "HH:mm"
            
            headerCell.dojoNameLbl.text = session!.dojo.name
            headerCell.sessionDateLbl.text = dateFormatter.string(from: session!.startDate) + " — " + timeFormatter.string(from: session!.endDate)
            
            return headerCell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "athleteTableCell", for: indexPath)
            
            guard let athleteCell = cell as? AttendanceTableViewCell else {
                fatalError("The dequeued cell is not an instance of AthletesTableViewCell.")
            }
            
            let athlete = athletes[indexPath.row]
            
            athleteCell.nomeLbl.text = athlete.name
            athleteCell.photoImg.image = athlete.photo
            athleteCell.idadeLbl.text = String(athlete.age) + " " + NSLocalizedString("years-old", comment: "")
            athleteCell.graduaçãoLbl.text = athlete.grade
            
            // Set gray background in alternate rows
            if indexPath.row % 2 != 0 {
                cell.backgroundColor = UIColor(colorLiteralRed: 250/255, green: 250/255, blue: 250/255, alpha: 1)
            }
            
            return athleteCell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 70 : 140
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let _ = session {
            session!.watchedAthletes.removeAll()
            session!.partialAthletes.removeAll()
            session!.missedAthletes.removeAll()
            session!.presentAthletes.removeAll()
            
            for i in 0 ... athletes.count - 1 {
                let index = IndexPath(row: i, section: 1)
                guard let athleteCell = tableView.cellForRow(at: index) as? AttendanceTableViewCell else {
                    fatalError("The dequeued cell is not an instance of AthletesTableViewCell.")
                }
                
                switch athleteCell.presençaBtns.selectedSegmentIndex {
                case 0:
                    session!.watchedAthletes.insert(athletes[i])
                case 1:
                    session!.partialAthletes.insert(athletes[i])
                case 2:
                    session!.missedAthletes.insert(athletes[i])
                case 3:
                    session!.presentAthletes.insert(athletes[i])
                default:
                    print("Selected segment index out of bounds…")
                }
            }
        }
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
}
