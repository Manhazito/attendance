//
//  Athlete.swift
//  attendance
//
//  Created by Filipe Ramos on 09/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit


class Athlete: NSObject, NSCoding {
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("atletas")
    
    //MARK: Properties
    struct Graduação {
        static let kyu6 = "6º Kyu"
        static let kyu5 = "5º Kyu"
        static let kyu4 = "4º Kyu"
        static let kyu3 = "3º Kyu"
        static let kyu2 = "2º Kyu"
        static let kyu1 = "1º Kyu"
        static let dan1 = "1º Dan"
        static let dan2 = "2º Dan"
        static let dan3 = "3º Dan"
        static let dan4 = "4º Dan"
        static let dan5 = "5º Dan"
    }
    
    struct PropertyKey {
        static let name = "name"
        static let age = "age"
        static let grade = "grade"
        static let photo = "photo"
    }
    
    var name: String
    var age: Int
    var grade: String
    var photo: UIImage
    
    init(name: String, age: Int, grade: String, photo: UIImage?) {
        self.name = name
        self.age = age
        self.grade = grade
//        self.photo = photo ?? UIImage(named: NSLocalizedString("default-photo", comment: "Photo Placeholder"))!
        self.photo = photo ?? UIImage(named: "AtleteImagePlaceholder")!
    }
    
    //MARK: - Coding
    required convenience init?(coder aDecoder: NSCoder) {
        guard let nome = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            print("Unable to decode the name for a Athlete object.")
            return nil
        }
        
        let idade = aDecoder.decodeInteger(forKey: PropertyKey.age)
        
        guard let graduação = aDecoder.decodeObject(forKey: PropertyKey.grade) as? String else {
            print("Unable to decode the grade for a Athlete object.")
            return nil
        }
        
        guard let foto = aDecoder.decodeObject(forKey: PropertyKey.photo) as? UIImage else {
            print("Unable to decode the photo for a Athlete object.")
            return nil
        }
        
        self.init(name: nome, age: idade, grade: graduação, photo: foto)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(age, forKey: PropertyKey.age)
        aCoder.encode(grade, forKey: PropertyKey.grade)
        aCoder.encode(photo, forKey: PropertyKey.photo)
    }
}


