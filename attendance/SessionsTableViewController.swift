//
//  SessionsTableViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 13/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class SessionsTableViewController: UITableViewController {
    //MARK: Properties
    var sessions = [TrainingSession]()
    var dojo: Dojo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero) // Hide empty cell lines
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadSessions()
    }
    
    
    //MARK: - Save & Load
    func loadSessions() {
        if let loadedSessions = NSKeyedUnarchiver.unarchiveObject(withFile: TrainingSession.ArchiveURL.path) as? [TrainingSession] {
            sessions = loadedSessions
//        } else {
//            let calendar = Calendar.current
//            var dateComponents = DateComponents()
//            dateComponents.year = 2017
//            dateComponents.month = 1
//            dateComponents.day = 14
//            dateComponents.hour = 18
//            dateComponents.minute = 30
//            let startDate = calendar.date(from: dateComponents)!
//            dateComponents.hour = 20
//            let endDate = calendar.date(from: dateComponents)!
//            let dojo = Dojo(named: "C.K.Cinfães")
//            
//            let s1 = TrainingSession(starting: startDate, ending: endDate, summary: "Strenghtning exercises in preparation for the upcomming competition.", where: dojo)
//            
//            trainingSessions = [s1]
//            
//            print("Dummy sessions created…")
//            saveSessions()
        }
        
        //MARK: todo: filter sessions based on the Dojo!
    }
    
    func saveSessions() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(sessions, toFile: TrainingSession.ArchiveURL.path)
        
        if isSuccessfulSave {
            print("Training Sessions successfully saved.")
        } else {
            //MARK: todo: Show error dialog.
            print("Failed to save Training Sessions…")
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sessions.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TrainingSessionCell", for: indexPath)
        
        cell.textLabel!.text = Utils.readableDateInterval(starting: sessions[indexPath.row].startDate, ending: sessions[indexPath.row].endDate)
        cell.detailTextLabel!.text = sessions[indexPath.row].summary
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .default, title: "Edit") { action, index in
            self.present(Utils.notImplementedDialog(), animated: true) {
                tableView.setEditing(false, animated: true) // Close cell
            }
            
            //MARK: todo: performSegueWithIdentifier, e.g.: self.performSegueWithIdentifier("showSessionDetails", sender: self)
        }
        edit.backgroundColor = UIColor.lightBlue
        
        let delete = UITableViewRowAction(style: .default, title: "Delete") { action, index in
            self.sessions.remove(at: index.row)
            tableView.deleteRows(at: [index], with: .fade)
        }
        delete.backgroundColor = UIColor.red
        
        return [edit, delete]
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "ShowSessionDetails" {
            guard let navigationController = segue.destination as? UINavigationController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let viewController = navigationController.viewControllers[0] as? AttendanceTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            viewController.session = sessions[indexPath.row]
            viewController.dojo = dojo
        }
    }
    
    @IBAction func unwindForSessionController(sender: UIStoryboardSegue) {
        if let senderCtrl = sender.source as? AttendanceTableViewController, let newSession = senderCtrl.session {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                sessions[selectedIndexPath.row] = newSession
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: sessions.count, section: 0)
                sessions.append(newSession)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
                
                saveSessions()
            }
        }
    }
}




