//
//  NewDojoTableViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 18/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class NewDojoTableViewController: UITableViewController {
    //MARK: Properties
    var athletes = [Athlete]()
    var dojo: Dojo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero) // Hide empty cell lines
        
        if let dojo = dojo {
            athletes = Array(dojo.athletes)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let dojo = dojo {
            let indexPath = IndexPath(row: 0, section: 0)
            guard let headerCell = tableView.cellForRow(at: indexPath) as? NewDojoHeaderTableViewCell else {
                fatalError("Could not reach table header.")
            }
            
            headerCell.dojoName!.text = dojo.name
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 1:
            return athletes.count
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewDojoHeaderCell", for: indexPath)
            
            guard let headerCell = cell as? NewDojoHeaderTableViewCell else {
                fatalError("The dequeued cell is not an instance of NewDojoHeaderTableViewCell.")
            }
            
            return headerCell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewDojoAthleteCell", for: indexPath)
            let athlete = athletes[indexPath.row]
            cell.textLabel?.text = athlete.name
            cell.detailTextLabel?.text = athlete.grade
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewDojoButtonCell", for: indexPath)
            
            return cell
        default:
            fatalError("Trying to show a session that does not exist: \(indexPath.section)")
        }
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 0 ? 96 : tableView.rowHeight
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return indexPath.section == 1
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            athletes.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        let indexPath = IndexPath(row: 0, section: 0)
        guard let headerCell = tableView.cellForRow(at: indexPath) as? NewDojoHeaderTableViewCell, let name = headerCell.dojoName!.text else {
            fatalError("Could not reach table header.")
        }
        
        dojo = Dojo(named: name, with: athletes)
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        if presentingViewController is UINavigationController {
            dismiss(animated: true, completion: nil)
        } else if let navController = navigationController {
            navController.popViewController(animated: true)
    	}
    }
    
    @IBAction func unwindToNewDojo(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? NewAthleteViewController, let athlete = sourceViewController.athlete {
            let newIndexPath = IndexPath(row: athletes.count, section: 1)
            athletes.append(athlete)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }
}
