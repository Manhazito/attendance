//
//  TrainingSession.swift
//  attendance
//
//  Created by Filipe Ramos on 13/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class TrainingSession: NSObject, NSCoding {
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("trainingSessions")
    
    //MARK: Properties
    struct PropertyKey {
        static let startDate = "startDate"
        static let endDate = "endDate"
        static let summary = "summary"
        static let dojo = "dojo"
        
        static let watchedAthletes = "watchedAthletes"
        static let partialAthletes = "partialAthletes"
        static let missedAthletes = "missedAthletes"
        static let presentAthletes = "presentAthletes"
    }
    
    var startDate: Date
    var endDate: Date
    var summary: String
    var dojo: Dojo
    var watchedAthletes = Set<Athlete>()
    var partialAthletes = Set<Athlete>()
    var missedAthletes = Set<Athlete>()
    var presentAthletes = Set<Athlete>()
    
    convenience init(starting startDate: Date, ending endDate: Date, where dojo: Dojo) {
        self.init(starting: startDate, ending: endDate, summary: "…", where: dojo)
    }
    
    init(starting startDate: Date, ending endDate: Date, summary: String, where dojo: Dojo) {
        self.startDate = startDate
        self.endDate = endDate
        self.summary = summary
        self.dojo = dojo
    }
    
    //MARK: - Coding
    required convenience init?(coder aDecoder: NSCoder) {
        guard let startDate = aDecoder.decodeObject(forKey: PropertyKey.startDate) as? Date else {
            print("Unable to decode the starting date for a Training Session object.")
            return nil
        }
        
        guard let endDate = aDecoder.decodeObject(forKey: PropertyKey.endDate) as? Date else {
            print("Unable to decode the ending date for a Training Session object.")
            return nil
        }
        
        guard let summary = aDecoder.decodeObject(forKey: PropertyKey.summary) as? String else {
            print("Unable to decode the summary for the Training Session object.")
            return nil
        }
        
        guard let dojo = aDecoder.decodeObject(forKey: PropertyKey.dojo) as? Dojo else {
            print("Unable to decode the Dojo for the Training Session object.")
            return nil
        }
        
        self.init(starting: startDate, ending: endDate, summary: summary, where: dojo)
        
        if let watchedAthletes = aDecoder.decodeObject(forKey: PropertyKey.watchedAthletes) as? Set<Athlete> {
            self.watchedAthletes = watchedAthletes
        }
        if let partialAthletes = aDecoder.decodeObject(forKey: PropertyKey.partialAthletes) as? Set<Athlete> {
            self.partialAthletes = partialAthletes
        }
        if let missedAthletes = aDecoder.decodeObject(forKey: PropertyKey.missedAthletes) as? Set<Athlete> {
            self.missedAthletes = missedAthletes
        }
        if let presentAthletes = aDecoder.decodeObject(forKey: PropertyKey.presentAthletes) as? Set<Athlete> {
            self.presentAthletes = presentAthletes
        }
    }
    
    //MARK: - Initializer
    func encode(with aCoder: NSCoder) {
        aCoder.encode(startDate, forKey: PropertyKey.startDate)
        aCoder.encode(endDate, forKey: PropertyKey.endDate)
        aCoder.encode(summary, forKey: PropertyKey.summary)
        aCoder.encode(dojo, forKey: PropertyKey.dojo)
        aCoder.encode(watchedAthletes, forKey: PropertyKey.watchedAthletes)
        aCoder.encode(partialAthletes, forKey: PropertyKey.partialAthletes)
        aCoder.encode(missedAthletes, forKey: PropertyKey.missedAthletes)
        aCoder.encode(presentAthletes, forKey: PropertyKey.presentAthletes)
    }
}



