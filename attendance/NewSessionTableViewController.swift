//
//  NewSessionTableViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 20/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class NewSessionTableViewController: UITableViewController {
    //MARK: Properties
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var summaryTxtView: UITextView!
    
    var dojo: Dojo?
    var session: TrainingSession?
    
    private var showStartDatePicker = false
    private var showEndDatePicker = false
    private var startDate: Date?
    private var endDate: Date?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero) // Hide empty cell lines
        
        guard let _ = dojo else {
            fatalError("Dojo not defined")
        }
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 && !showStartDatePicker || indexPath.row == 3 && !showEndDatePicker {
            return 0
        }
        
        return super.tableView(tableView, heightForRowAt: indexPath)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            showStartDatePicker = !showStartDatePicker
            tableView.beginUpdates()
            tableView.endUpdates()
        } else if indexPath.row == 2 {
            showEndDatePicker = !showEndDatePicker
            tableView.beginUpdates()
            tableView.endUpdates()
        }
    }

    // MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if let _ = startDate {
            if let _ = endDate {
                return true
            } else {
                endDateLbl.shake()
                return false
            }
        } else {
            startDateLbl.shake()
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        session = TrainingSession(starting: startDate!, ending: endDate!, summary: summaryTxtView.text, where: dojo!)
    }
    
    @IBAction func dismiss(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

    
    //MARK: - Actions
    @IBAction func startDateChanged(_ sender: UIDatePicker) {
        startDateLbl.text = "Starts: " + Utils.readableDate(sender.date)
        startDate = sender.date
    }
    
    @IBAction func endDateChanged(_ sender: UIDatePicker) {
        endDateLbl.text = "Ends: " + Utils.readableDate(sender.date)
        endDate = sender.date
    }
}
