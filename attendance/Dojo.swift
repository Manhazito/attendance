//
//  Dojo.swift
//  attendance
//
//  Created by Filipe Ramos on 18/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class Dojo: NSObject, NSCoding {
    //MARK: Archiving Paths
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("dojos")
    
    //MARK: Properties
    struct PropertyKey {
        static let name = "name"
        static let athletes = "athletes"
    }

    var name: String
    var athletes = Set<Athlete>()

    init(named name: String) {
        self.name = name
    }
    
    convenience init(named name: String, with athletes: [Athlete]) {
        self.init(named: name)
        self.athletes = Set(athletes)
    }
    

    //MARK: - Coding
    required convenience init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: PropertyKey.name) as? String else {
            print("Unable to decode the name for the Dojo object.")
            return nil
        }
        
        if let athletes = aDecoder.decodeObject(forKey: PropertyKey.athletes) as? Set<Athlete> {
            self.init(named: name, with: Array(athletes))
        } else {
            self.init(named: name)
        }
	}
    
    //MARK: - Initializer
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: PropertyKey.name)
        aCoder.encode(athletes, forKey: PropertyKey.athletes)
    }
}
