//
//  DojosTableViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 18/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class DojosTableViewController: UITableViewController {
    //MARK: Properties
    var dojos = [Dojo]()

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: .zero) // Hide empty cell lines
        loadDojos()
	}
    

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dojos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DojoTableViewCell", for: indexPath)

        cell.textLabel?.text = dojos[indexPath.row].name

        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let edit = UITableViewRowAction(style: .default, title: "Edit") { action, index in
            tableView.setEditing(false, animated: true) // Close cell
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: UITableViewScrollPosition.none)
            self.performSegue(withIdentifier: "EditDojo", sender: tableView.cellForRow(at: indexPath))
        }
        edit.backgroundColor = UIColor.lightBlue
        
        let delete = UITableViewRowAction(style: .default, title: "Delete") { action, index in
            self.dojos.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            self.saveDojos()
        }
        delete.backgroundColor = UIColor.red
        
        return [edit, delete]
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
    }


    //MARK: - Save & Load
    func loadDojos() {
        if let loadedDojos = NSKeyedUnarchiver.unarchiveObject(withFile: Dojo.ArchiveURL.path) as? [Dojo] {
            dojos = loadedDojos
        }
    }
    
    func saveDojos() {
        let dojosNoRepeat = Array(Set(dojos))
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(dojosNoRepeat, toFile: Dojo.ArchiveURL.path)
        
        if isSuccessfulSave {
            print("Dojos successfully saved.")
        } else {
            print("Failed to save Dojos…")
        }
    }
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == "ShowDojoSessions" {
            guard let viewController = segue.destination as? SessionsTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            viewController.dojo = dojos[indexPath.row]
        } else if segue.identifier == "EditDojo" {
            guard let viewController = segue.destination as? NewDojoTableViewController else {
                fatalError("Unexpected destination: \(segue.destination)")
            }
            guard let selectedCell = sender as? UITableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }
            guard let indexPath = tableView.indexPath(for: selectedCell) else {
                fatalError("The selected cell is not being displayed by the table")
            }
            
            viewController.dojo = dojos[indexPath.row]
        }
    }
    
    @IBAction func unwindToDojoList(sender: UIStoryboardSegue) {
        if let sourceViewController = sender.source as? NewDojoTableViewController, let dojo = sourceViewController.dojo {
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                dojos[selectedIndexPath.row] = dojo
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: dojos.count, section: 0)
                dojos.append(dojo)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
            
            saveDojos()
        }
    }
}



