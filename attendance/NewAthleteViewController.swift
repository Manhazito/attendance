//
//  NewAthleteViewController.swift
//  attendance
//
//  Created by Filipe Ramos on 09/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class NewAthleteViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var photoImg: UIImageView!
    @IBOutlet weak var nomeTxt: UITextField!
    @IBOutlet weak var idadeTxt: UITextField!
    @IBOutlet weak var graduaçãoPicker: UIPickerView!
    
    
    //MARK: Properties
    var athlete: Athlete?
    var grades = [Athlete.Graduação.kyu6, Athlete.Graduação.kyu5, Athlete.Graduação.kyu4, Athlete.Graduação.kyu3, Athlete.Graduação.kyu2, Athlete.Graduação.kyu1, Athlete.Graduação.dan1, Athlete.Graduação.dan2, Athlete.Graduação.dan3, Athlete.Graduação.dan4, Athlete.Graduação.dan5]
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        graduaçãoPicker.delegate = self
        graduaçãoPicker.dataSource = self
        
        initFields()
    }
    
    func initFields() {
        if let _ = athlete {
            photoImg.image = athlete!.photo
            nomeTxt.text = athlete!.name
            idadeTxt.text = String(athlete!.age)
            graduaçãoPicker.selectRow(grades.index(of: athlete!.grade)!, inComponent: 0, animated: true)
        }
    }
    
    //MARK: - Grade Picker Delegate & Data Source
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return grades.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return grades[row]
    }
    
    //MARK: - Navigation
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "SaveAthleteDetails" {
            if let _ = nomeTxt.text, let idadeStr = idadeTxt.text, let _ = Int(idadeStr) {
                return true
            } else {
                print("Could not save athlete: some data countains errors…")
                checkErrors()
                
                return false
            }
        }
        
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let nome = nomeTxt.text, let idadeStr = idadeTxt.text, let idade = Int(idadeStr) {
            let graduação = grades[graduaçãoPicker.selectedRow(inComponent: 0)]
            let photo = photoImg.image
            athlete = Athlete(name: nome, age: idade, grade: graduação, photo: photo)
        } else {
            print("Could not save athlete: some data countains errors…")
        }
    }
    
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        if presentingViewController is UINavigationController {
            dismiss(animated: true, completion: nil)
        } else if let navController = navigationController {
            navController.popViewController(animated: true)
        }
    }

    
    //MARK: - Validation
    func checkErrors() {
        // Check name
        guard let nome = nomeTxt.text else {
            nomeTxt.shake()
            return
        }
        if nome.characters.count < 5 {
            nomeTxt.shake()
            return
        }
        
        // Check age
        guard let idadeStr = idadeTxt.text else {
            idadeTxt.shake()
            return
        }
        guard let idade = Int(idadeStr) else {
            idadeTxt.shake()
            return
        }
        if idade >= 5 && idade <= 140 {
            idadeTxt.shake()
            return
        }
    }
    
    //MARK: - Image Picker
    @IBAction func selectPhotoImage(_ sender: UITapGestureRecognizer) {
        nomeTxt.resignFirstResponder() // Hide keyboard…
        
        let imagePickerController = UIImagePickerController();
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        guard let selectedImage = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        photoImg.image = selectedImage
        dismiss(animated: true, completion: nil)
    }
}


