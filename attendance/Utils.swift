//
//  Utils.swift
//  attendance
//
//  Created by Filipe Ramos on 17/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

extension UIColor {
    @nonobjc static let lightBlue = UIColor(red: 0x1B/255.0, green: 0x8F/255.0, blue: 0xFE/255.0, alpha:1.0)
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
}

class Utils {
    static func readableDateInterval(starting startDate: Date, ending endDate: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        let timeFormatter = DateFormatter()
        timeFormatter.dateStyle = .none
        timeFormatter.timeStyle = .short
        
		return dateFormatter.string(from: startDate) + " (" + timeFormatter.string(from: startDate) + " - " + timeFormatter.string(from: endDate) + ")"
    }
    
    static func readableDate(_ date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date)
    }
    
    static func notImplementedDialog() -> UIAlertController {
        let title = "Not implemented"
        let message = "This feature is not implemented yet."
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
        alertController.addAction(okAction)
        
        return alertController
    }
}
