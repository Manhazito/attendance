//
//  AttendanceTableViewCell.swift
//  attendance
//
//  Created by Filipe Ramos on 09/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class AttendanceTableViewCell: UITableViewCell {

    //MARK: Properties
    @IBOutlet weak var photoImg: UIImageView!
    @IBOutlet weak var graduaçãoLbl: UILabel!
    @IBOutlet weak var idadeLbl: UILabel!
    @IBOutlet weak var nomeLbl: UILabel!
    @IBOutlet weak var presençaBtns: UISegmentedControl!
    
}
