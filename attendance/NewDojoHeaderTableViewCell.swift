//
//  NewDojoHeaderTableViewCell.swift
//  attendance
//
//  Created by Filipe Ramos on 19/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class NewDojoHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var dojoName: UITextField!

}
