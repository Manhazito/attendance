//
//  AttendanceTableViewHeaderCell.swift
//  attendance
//
//  Created by Filipe Ramos on 16/01/17.
//  Copyright © 2017 Filipe Ramos. All rights reserved.
//

import UIKit

class AttendanceTableViewHeaderCell: UITableViewCell {
    
    //MARK: Properties
    @IBOutlet weak var dojoNameLbl: UILabel!
    @IBOutlet weak var sessionDateLbl: UILabel!
    
}

